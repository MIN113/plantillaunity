﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyUP : MonoBehaviour
{
	public float velx;
	private float moveHorizontal;
	private Vector3 positionActual;
	public float maxx;
	public Transform posBall;

	private void Update()
	{
		if(posBall.position.y > 0)
		{
			if(posBall.position.x > transform.position.x)
			{
				moveHorizontal = 1.0f;
			}
			if(posBall.position.x < transform.position.x)
			{
				moveHorizontal = -1.0f;
			}
		}else if (posBall.position.y < 0)
		{
			moveHorizontal = 0.0f;
		}



		transform.Translate( velx * moveHorizontal * Time.deltaTime, 0, 0);
		positionActual = transform.position;

		if(positionActual.x >= maxx)
		{
			positionActual.x = maxx;
		}

		if(positionActual.x <= -maxx)
		{
			positionActual.x = -maxx;
		}
		//}

		transform.position = positionActual;
	}
}