﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Ball : MonoBehaviour {

    public Vector2 speed;
    public float maxVel;
	Vector2 originalPos;

	
	void Start () {
		originalPos = new Vector2 (transform.position.x, transform.position.y);
	}

	void Update () {
        

        transform.Translate(speed.x * Time.deltaTime, speed.y * Time.deltaTime, 0);
		}


    void OnTriggerEnter(Collider other){
		
		speed.x *= 1.25f;
		speed.y *= 1.25f;

		if (speed.x > maxVel) {
			speed.x = maxVel;
		}
		if (speed.y > maxVel) {
			speed.y = maxVel;
		}
		if (speed.x < -maxVel) {
			speed.x = -maxVel;
		}
		if (speed.y < -maxVel) {
			speed.y = -maxVel;
		}

		switch (other.tag) {

		case "UP":
			transform.position = originalPos;
			speed.x=-3;
			speed.y=-1;
			//speed.y = -speed.y;
			break;
		case "DOWN":
			transform.position = originalPos;
			speed.x=3;
			speed.y=1;
			//speed.y = -speed.y;
			break;
		case "LEFT":
			transform.position = originalPos;
			speed.x=-3;
			speed.y=1;
			//speed.x = -speed.x;
			break;
		case "RIGHT":
			transform.position = originalPos;
			speed.x=3;
			speed.y=-1;
			//speed.x = -speed.x;
			break;



		case "Player":
			speed.x = -speed.x;
			break;
		case "Player2":
			speed.x = -speed.x;
			break;
		case "Player3":
			speed.y = -speed.y;
			break;
		case "Player4":
			speed.y = -speed.y;
			break;

		}

    }
}
