﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player : MonoBehaviour
{
    public float vely;
    private float moveVertical;
    private Vector3 positionActual;
    public float maxy;


    void Start() {

    }

    private void Update()
    {
        //Transform t = GetComponent<Transform>();//

        moveVertical = Input.GetAxis("Vertical");
        transform.Translate(0, vely * moveVertical * Time.deltaTime, 0);
        positionActual = transform.position;

        //if (moveVertical > 0 || moveVertical < 0){
        //    positionActual.y = positionActual.y + vely * moveVertical * Time.deltaTime;

        if(positionActual.y >= maxy){
            positionActual.y = maxy;
        }

        if(positionActual.y <= -maxy){
            positionActual.y = -maxy;
        }
        //}

        transform.position = positionActual;
    }
}