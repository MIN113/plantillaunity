﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enremy : MonoBehaviour
{
    public float vely;
    private float moveVertical;
    private Vector3 positionActual;
    public float maxy;
    public Transform posBall;

    private void Update()
    {
        if(posBall.position.x > 0)
        {
            if(posBall.position.y > transform.position.y)
            {
                moveVertical = 1.0f;
            }
            if(posBall.position.y < transform.position.y)
            {
                moveVertical = -1.0f;
            }
        }else if (posBall.position.x < 0)
        {
			moveVertical = 0.0f;
        }



        transform.Translate(0, vely * moveVertical * Time.deltaTime, 0);
        positionActual = transform.position;

        if(positionActual.y >= maxy)
        {
            positionActual.y = maxy;
        }

        if(positionActual.y <= -maxy)
        {
            positionActual.y = -maxy;
        }
        //}

        transform.position = positionActual;
    }
}